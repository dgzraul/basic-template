import config from '~/config/config';
import express from "express";
import { __load, __life } from '~/@server/_server';
import bodyParser from 'body-parser';
import timeout from 'connect-timeout';
import routes from '../routes/index';
import { ApolloServer } from "apollo-server-express";
import { ErrorMiddleware } from '!/middlewares/error';

/**
 * @description Control exceptions without Express
*/
process.on('unhandledRejection', (error) => {
  console.log("--> unhandledRejection::", error);
});

process.on('uncaughtException', (error) => {
  console.log("--> uncaughtException::", error);
});


/**
 * @description Express config and load middlewares
*/
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', config.viewEngine);
app.set('views', config.views);
app.use('/assets', express.static(config.assets));
app.use(timeout(15000));


/**
 * @description Static routes
*/
app.use('/', routes);

// GraphQL
import typeDefs from "../types/_vendor";
import resolvers from "../resolvers/_vendor";

const GraphQL = new ApolloServer({
  typeDefs,
  resolvers,
  debug: config.debuggerGQL,
  playground: config.playgroundGQL,
});
GraphQL.applyMiddleware({ app, path: '/api' });

/**
 * @description Not found resource
*/
app.use((request, response, next) => {
  let error = new Error('Not Fount');
  error.status = 404;
  next(error);
});


/**
 * @desription Error middleware
*/
app.use(ErrorMiddleware.parse);


/**
 * @description Life server
*/
var server = __load(app);
__life(server);
