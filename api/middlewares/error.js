import { APIService } from '~/config/info';

export class ErrorMiddleware {
  constructor() {}

  static parse(error, request, response, next){
    let errorObject = {
      status: error.status || 500,
      name: error.name,
      message: error.message,
      APIService: APIService
    };
    response.status(errorObject.status).json(errorObject);
  }
}
