/**
 * @description Project information
 * @property project - Project name
 * @property version - Project version
 * @property contact - Support unit
 * @property support - Contact information
*/
const APIService = {
  project: 'Project-Name',

  description: 'Description project',

  version: 'v0.0.0',

  contact: 'Enterprise name',

  support: [

    'your-name <your-email@domain.example>'

  ]
}

/**
 * @description Export project information
 * @export APIService
*/
export default APIService;
export { APIService }
