/**
 * @description Database config  :: PostgreSQL
 * @property host - Address
 * @property user - User name
 * @property password - Private key
 * @property database - Database name
 * @property schema - Schema name
 * @property port - Port number (default 5432)
*/
const DBDefault = {
  host: process.env.DB_DEFAULT_HOST,

  user: process.env.DB_DEFAULT_USER,

  password: process.env.DB_DEFAULT_PASSWORD,

  database: process.env.DB_DEFAULT_DATABASE,

  port: process.env.DB_DEFAULT_PORT || 5432
}

/**
 * @description Export config
 * @export Database properties
*/
export default DBDefault;
export { DBDefault }
