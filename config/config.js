import path from 'path';

/**
 * @desription General config
 * @property viewEngine Motor views
 * @property views Folder with views
 * @property assets Public folder with assets
 * @property https SSLCertificats paths
*/
const config = {
  server: {

    environment: process.env.NODE_ENV,

    port: process.env.PORT,

    https: {

      enabled: false, // Active certificates

      key: path.join(process.cwd(), '/config/ssl-certificates/key-dev.pem'), // Certificate file

      cert: path.join(process.cwd(), '/config/ssl-certificates/cert-dev.pem'), // Certificate file
    }

  },

  viewEngine: 'pug', // By default pug is installed

  views: path.join(process.cwd(), '/views'), // Views folders

  assets: path.join(process.cwd(), '/assets'), // Public folder

  playgroundGQL: true, // UI graphics users

  debuggerGQL: process.env.NODE_ENV == 'development' ? true : false, // Stack trace errors

  introspectionGQL: true,

  tracingGQL: process.env.NODE_ENV == 'development' ? true : false,

  logs: process.env.NODE_ENV == 'development' ? true : false,

  applications: {

    telegram: {

      token: process.env.TELEGRAM_KEY || '',

      chat_id: process.env.CHAT_ID || ''

    },

    unique_key: process.env.UNIQUE_KEY || ''

  }

}

/**
 * @export Export config
*/
export default config;
export { config }
