if(!process.env.NODE_ENV || !process.env.PORT){
  console.error("\n\n--------------------------------------------------------------------------------");
  console.error("--------------------------------------------------------------------------------\n\n");
  console.error("      Enviroment variables are missing                                              ");
  console.error("      You can see the enviroments variables in the README file                      ");
  console.error("      Press Ctrl+C to quit ...                                                      ");
  console.error("\n\n--------------------------------------------------------------------------------");
  console.error("--------------------------------------------------------------------------------\n\n\n");
  throw new Error('Enviroment variables are missing!');
}else{
  require('@babel/register');
  require('./api/_server/server.js');
}
