import config from '~/config/config';
import ip from './_ip';

/**
 * @decription Print life serer with info
*/
const printBootBanner = () => {
  let protocol = config.server.https.enabled ? 'https' : 'http';
  // serverRunning = `Server running at http://${getIPAddress}:${server.address().port}`;
  console.log("\n\n--------------------------------------------------------------------------------");
  console.log("--------------------------------------------------------------------------------\n\n");
  console.log(`      Server running at ${protocol}://${ip}:${config.server.port}`);
  console.log(`      Enviroment: [${process.env.NODE_ENV}]`);
  console.log("      Press Ctrl+C to quit....                       dgzraul.web@gmail.com");
  console.log("\n\n--------------------------------------------------------------------------------");
  console.log("--------------------------------------------------------------------------------\n\n\n");
}

/**
 * @export printBootBanner
*/
export default printBootBanner;
export { printBootBanner };
