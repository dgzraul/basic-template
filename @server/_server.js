import config from '~/config/config';
import printBootBanner from './_printBootBanner';

const __load = (__express) => {
  let server;
  if(config.server.https.enabled){
    let https = require('https');
    let fs = require('fs');
    server = https.createServer({
      key: (() => {
        try {
          return fs.readFileSync(config.server.https.key);
        } catch (e) {
          throw new Error(e);
        }
      })(),
      cert: (() => {
        try {
          return fs.readFileSync(config.server.https.cert);
        } catch (e) {
          throw new Error(e);
        }
      })()
    }, __express);
  } else {
    let http = require('http');
    server = http.createServer(__express);
  }
  return server;
}

const __life = (__server) => {
  __server.listen(config.server.port, () => {
    printBootBanner();
  });
}


export { __load, __life };
