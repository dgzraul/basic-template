FROM node:12.14.1-alpine3.9

WORKDIR /usr/src/

COPY package.* .

RUN npm install

COPY . .

EXPOSE 8080

CMD [ "npm","run","start:dev" ]